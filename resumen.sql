create table resumen (idresumen integer unique not null, numerot integer not null, inicioperiodo date not null, finperiodo date not null, cantidadcompras integer not null, 
fvenc date not null, montototal real not null);

alter table resumen add constraint resumen_pk primary key (idresumen);
alter table resumen add constraint resumen_fk foreign key (numerot) references tarjeta(numerotarjeta);
