create table cliente (
    dni         integer unique not null, 
    nombre      varchar(64) not null, 
    apellido    VARCHAR(64) not null, 
    telefono    INTEGER not null, 
    direc       varchar(64) not null);

alter table cliente add CONSTRAINT cliente_pk primary key (dni);
