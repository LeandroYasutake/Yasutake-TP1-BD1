create table compra (
	idcompra        integer unique not null,
	fecha           TIMESTAMP not null,
	monto           REAL not null,
	comercioid      integer not null,
	cp              integer not null,
	numero          integer not null,
	dni             integer not null,
);

alter table compra add CONSTRAINT compra_pk primary key (idcompra);
alter table compra add CONSTRAINT compra_fk FOREIGN key (numero) REFERENCES tarjeta(numerotarjeta);
alter table compra add CONSTRAINT compra_fk2 FOREIGN key (dni) REFERENCES cliente(dni);
