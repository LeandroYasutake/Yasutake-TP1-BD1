CREATE TABLE detallecompra(
    detalleID int unique not null,
    compraID int not null,
    productoID int not null,
    cantidadProductos int not null,
    precioUnitario int not null);

ALTER TABLE detallecompra ADD CONSTRAINT detallecomprapk PRIMARY KEY(detalleID);
ALTER TABLE detallecompra ADD CONSTRAINT comprafk FOREIGN KEY(compraID) REFERENCES compra(idcompra);
