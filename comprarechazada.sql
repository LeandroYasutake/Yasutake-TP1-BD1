create table comprarechazada (idrechazo integer unique not null, numerotarjeta integer not null, idcompra integer not null);

alter table comprarechazada add constraint rechazo_pk primary key (idrechazo);
alter table comprarechazada add constraint tarjeta_fk foreign key (numerotarjeta) references tarjeta (numerotarjeta);
alter table comprarechazada add constraint compra_fk foreign key (idcompra) references compra(idcompra);
